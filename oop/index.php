    <?php
    include "frog.php";
    include "ape.php";

    $n = "Name : ";
    $l = "Legs : ";
    $c = "cold blooded :";


    $sheep = new Animal("shaun");
    echo "$n $sheep->name <br>"; // "shaun"
    echo "$l $sheep->legs <br>" ; // 4
    echo " $c $sheep->cold_blooded <br> <br>"; // "no"

    $kodok = new Frog("buduk");
    echo "$n $kodok->name <br>"; 
    echo "$l $kodok->legs <br>" ;
    echo " $c $kodok->cold_blooded <br>";
    $kodok->jump() ; // "hop hop"    

    echo "<br> <br>";
    $sungokong = new Ape("kera sakti");
    echo "$n $sungokong->name <br>"; 
    echo "$l $sungokong->legs <br>" ;
    echo " $c $sungokong->cold_blooded <br>";
    $sungokong->yell() // "Auooo"
    ?>

    
